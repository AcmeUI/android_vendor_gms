#
# Copyright (C) 2022 Acme
#
# SPDX-License-Identifier: Apache-2.0
#

# Core
ifneq ($(findstring $(WITH_GMS)$(WITH_GMS_CORE), true true),)
$(call inherit-product, vendor/gms/core/core.mk)
endif


# Extras
ifeq ($(WITH_GMS), true)
$(call inherit-product, vendor/gms/extras/extras.mk)
endif

# Gboard
PRODUCT_PACKAGES += \
    LatinIMEGooglePrebuilt
